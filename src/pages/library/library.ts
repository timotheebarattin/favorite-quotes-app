import { Component, OnInit } from '@angular/core';

import { Quote } from '../../data/quote.interface';
import quotes from '../../data/quotes';

import { CategoryPage } from './category/category';

@Component({
  selector: 'page-library',
  templateUrl: 'library.html',
})
export class LibraryPage implements OnInit {
  quoteCollection: {category: string, quotes: Quote[], icon: string}[];
  categoryPage = CategoryPage;

  ngOnInit(){
    this.quoteCollection = quotes;
  }
}
