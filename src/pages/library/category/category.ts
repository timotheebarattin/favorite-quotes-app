import { Component, OnInit } from "@angular/core";
import { NavParams, AlertController } from "ionic-angular";

import { Quote } from '../../../data/quote.interface';
import { QuotesService } from "../../../services/quotes";

@Component({
    selector: 'page-category',
    templateUrl: 'category.html',
})
export class CategoryPage implements OnInit {
    quoteGroup: {category: string, quotes: Quote[], icon: string}

    constructor(private navParams: NavParams, private alertCtrl: AlertController, private quotesService: QuotesService) {}

    ngOnInit() {
        this.quoteGroup = this.navParams.data;
    }

    onAddToFavorites(selectedQuote: Quote) {
        const alert = this.alertCtrl.create({
            title: 'Add Quote', 
            subTitle: 'Are you sure?', 
            message: 'Are you sure you want to add the quote to favorites?',
            buttons: [
                {
                    text: 'Yes, go ahead!',
                    handler: () => {
                        this.quotesService.addQuoteToFavorites(selectedQuote);
                    }
                },
                {
                    text: 'Nope, my mistake.',
                    role: 'cancel',
                    handler: () => {
                        console.log('Nevermind')
                    }
                }
            ]
        });

        alert.present();
    }

    onRemoveFromFavorites(quote: Quote) {
        this.quotesService.removeQuoteFromFavorites(quote);
    }

    isFavorite(quote: Quote) {
        return this.quotesService.isQuoteFavorite(quote);
    }

}