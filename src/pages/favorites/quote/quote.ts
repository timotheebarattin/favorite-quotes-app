import { Component } from "@angular/core";

import { ViewController, NavParams, AlertController } from "ionic-angular";

import { Quote } from '../../../data/quote.interface';
import { QuotesService } from '../../../services/quotes'

@Component({
    selector: 'page-quote',
    templateUrl: 'quote.html',
})
export class QuotePage {
    person: string;
    text: string;
    quote: Quote;

    constructor(
        private viewCtrl: ViewController, 
        private navParams: NavParams, 
        private alertCtrl: AlertController,
        private quotesService: QuotesService
    ) {}

    onClose(remove = false) {
        this.viewCtrl.dismiss(remove);
    }

    ionViewDidLoad() {
        this.quote = this.navParams.data;
        this.person = this.navParams.get('person');
        this.text = this.navParams.get('text');
    }

    onRemoveFavorite(selectedQuote: Quote) {
        const alert = this.alertCtrl.create({
            title: 'Remove Quote', 
            subTitle: 'Are you sure?', 
            message: 'Are you sure you want to remove the quote from favorites?',
            buttons: [
                {
                    text: 'Yes, go ahead!',
                    handler: () => {
                        this.quotesService.removeQuoteFromFavorites(selectedQuote);
                    }
                },
                {
                    text: 'Nope, my mistake.',
                    role: 'cancel',
                    handler: () => {
                        console.log('Nevermind')
                    }
                }
            ]
        });

        alert.present();
    }

}