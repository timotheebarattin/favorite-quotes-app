export default [
    {
        category: 'inspirational',
        quotes: [
            {
                id: '1',
                person: 'Beyoncé',
                text: 'I searched around the world and found myself'
            },
            {
                id: '2',
                person: 'Mary Schmid',
                text: 'Do one thing that scares you everyday'
            },
        ],
        icon: 'bulb'
    },
    {
        category: 'motivational',
        quotes: [
            {
                id: '3',
                person: 'Jerry Rice',
                text: 'Today I will do what others won’t, so tomorrow I can accomplish what others can’t'
            },
            {
                id: '4',
                person: 'Somesmar Tass',
                text: 'Fake it until you become it: life is not about finding yourself, life is about creating yourself'
            },
        ],
        icon: 'battery-charging'
    },
    {
        category: 'truths',
        quotes: [
            {
                id: '5',
                person: 'Albert Camus',
                text: 'Ne pas aimer les autres c’est s’aimer trop soi-même'
            },
            {
                id: '6',
                person: 'Somesmar Tass',
                text: 'On ne choisit pas ses amis'
            },
            {
                id: '7',
                person: 'Charles Baudelaire',
                text: 'La plus belle des ruses du Diable est de vous persuader qu’il n’existe pas'
            },
        ],
        icon: 'glasses'
    },
]